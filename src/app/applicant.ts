export class Applicant {

  constructor(
    public firstname: string,
    public surname: string,
    public preferredName: string,
    public day: number,
    public month: string,
    public year: number,
    public gender: string,
    public passportNum: number,
    public nationality: string,
    public email: string,
    public contactNum: number,
    public houseNum: number,
    public houseAddress: string,
    public houseAddressTwo: string,
    public city: string,
    public region: string,
    public country: string,
    public zipPostcode: string,
    public firstLanguage: string,
    public currentOccupation: string,
    public currentOccuptionDetail: string,
    public university: string,
    public specificSupport: string, // Personal Details - End
    public emergencyName: string, // Emergency Contact - Begin
    public emergencyNumber: number,
    public emergencyEmail: string, // Emergency Contact - End
    public course: string, // Eligibility & Criteria - Begin
    public courseEligibilityCheck: boolean, // Eligibility & Criter - End
    public termsCheck: boolean,
    public howFoundOut: string,
    public howFoundOutDesc: string,
    public privacyCheck: boolean,
    public fileDocs = [{
      fileName: '',
      fileData: ''
    }]
  ) {}
}
