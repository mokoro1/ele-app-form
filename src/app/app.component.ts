import { Component, ViewChild, ErrorHandler, OnInit } from '@angular/core';
import { ToFlowService } from './to-flow.service';
import { UploadService } from './upload.service';

import { Applicant } from './applicant';
import { isNumber } from 'util';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { HttpClient } from 'selenium-webdriver/http';
import { browser } from 'protractor';
import { JsonPipe } from '@angular/common';
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
import { when } from 'q';
import { stringify } from '@angular/core/src/render3/util';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  @ViewChild('file') file;

  title = 'English Learning Education - Application';


  // Date of Birth
  day = [1, 2, 3, 4, 5 , 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
  month = ['January', 'February', 'March', 'April', 'May', 'June' , 'July', 'August', 'September', 'October', 'November', 'December'];
  year = [ 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1914, 1915, 1916, 1917, 1918, 1919,
    1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949,
    1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963,
    1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981,
    1982, 1983, 1984, 1985, 1986,
    1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003,
    2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019];



  // List of countries
  country = ['Afghanistan' , 'Albania', 'Algeria', 'Andorra', 'Angola', 'Anguilla',
  'Antigua & Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas'
  , 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize',
  'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia & Herzegovina', 'Botswana', 'Brazil', 'British Virgin Islands'
  , 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon',
  'Canada', 'Cape Verde', 'Cayman Islands', 'Chad', 'Chile', 'China', 'Colombia', 'Congo', 'Cook Islands', 'Costa Rica'
  , 'Cote D Ivoire', 'Croatia', 'Cruise Ship', 'Cuba', 'Cyprus', 'Czech Republic',
  'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea'
  , 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Fiji',
  'Finland', 'France', 'French Polynesia', 'French West Indies', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana'
  , 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea Bissau', 'Guyana', 'Haiti',
  'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India'
  , 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan',
  'Kazakhstan', 'Kenya', 'Kuwait', 'Kyrgyz Republic', 'Laos', 'Latvia'
  , 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar',
  'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Mauritania'
  , 'Mauritius', 'Mexico', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Namibia',
  'Nepal', 'Netherlands', 'Netherlands Antilles', 'New Caledonia'
  , 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Norway', 'Oman', 'Pakistan', 'Palestine', 'Panama',
  'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal'
  , 'Puerto Rico', 'Qatar', 'Reunion', 'Romania', 'Russia', 'Rwanda', 'Saint Pierre & Miquelon', 'Samoa', 'San Marino',
  'Satellite', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles'
  , 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'South Africa', 'South Korea', 'Spain', 'Sri Lanka', 'St Kitts & Nevis',
  'St Lucia', 'St Vincent', 'St. Lucia', 'Sudan'
  , 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand',
  'Timor L\'Este', 'Togo', 'Tonga', 'Trinidad & Tobago', 'Tunisia'
  , 'Turkey', 'Turkmenistan', 'Turks & Caicos', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States',
   'United States Minor Outlying Islands', 'Uruguay'
  , 'Uzbekistan', 'Venezuela', 'Vietnam', 'Virgin Islands (US)', 'Yemen', 'Zambia', 'Zimbabwe'];

  // Current Occupation
  currentOccupation = ['Student - Undergraduate Degree', 'Student - Postgraduate (including PhD and others)', 'Other'];


  // List of Courses
  course = ['Debating International Relations', 'Global Impact of Sport', 'Pre-University: Social Science',
  'Business Communications and Social Media'];

  howFoundOut = ['University of Edinburgh website', 'My home university', 'Online portal', 'Social Media', 'A friend/relative', 'Other'];






  constructor( private toFlowService: ToFlowService, private uploadService: UploadService ) { }
/** Instantiating a new Applicant with the object called User Model */
  userModel = new Applicant('', '', '', null , '', null, '', null, '',
  '', null, null, '', '', '', '', '', '', '', '', '', '', '', '', null,
  '', '', false, false, '', '', false, [{fileName: '',  fileData: ''},  {fileName: '', fileData: ''}, {fileName: '', fileData: ''}, {fileName: '',  fileData: ''}]);



/*
On FileDocs 2-Dimensional Array:

fileDocs[['', ''], ['', ''], ['', ''], ['','']]

*/


  public elem;
  public myReader: FileReader = new FileReader();
  public base64result;


/* Select files to upload */
  selectFile(event) {
  this.elem = event.target; // Element listening to click event in HTML


  if (this.elem.files.length > 0) {
    console.log(this.elem.files[0].name);
  }
  this.readThis(event.target);
}


/* Reads my File */
readThis(inputValue: any): void {
  const file: File = inputValue.files[0];

  this.myReader.onloadend = (e) => {
  this.file = this.myReader.result;
  this.base64result = /,(.+)/.exec(this.myReader.result.toString())[1]; // Strips type from base64 string to give us base64 only
  // console.log(this.base64result); // Prints only file Base64 to console
  // console.log(this.myReader.result); // Prints file details with base64 to console

  };
  this.myReader.readAsDataURL(file);
}


  onSubmit() {
    alert('Success! \nYour application has been submitted and will be reviewed by our staff.');



    const modifiedUserModel = {...this.userModel};


    let key = 'fileName';

    for (let i = 0; i < modifiedUserModel.fileDocs.length; i++) {
    // Update file values from userModel to include the base64 string content and the file name
      modifiedUserModel.fileDocs[i][key] = 'supporting_documents_' + i + '_' + this.elem.files[0].name;
      modifiedUserModel.fileDocs[i].fileData = this.base64result;
    }





    console.log(JSON.stringify(modifiedUserModel));
    console.log(Object.entries(modifiedUserModel.fileDocs));

    /** Submit everything to Flow */
    this.toFlowService.toFlow(modifiedUserModel)
    .subscribe(
      data => console.log('Success!', data),
      error => console.log('Error!', error)
    );
  }
}
