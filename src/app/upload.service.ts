import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Applicant } from './applicant';




@Injectable({
  providedIn: 'root'
})
export class UploadService {



  constructor(private http: HttpClient) { }

  url = 'https://prod-76.westeurope.logic.azure.com:443/workflows/3e57b775c1ff4847bd600f2e93e4ec98/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=3DpG7m5scqSRV7bvxMqF2jf7w8Lh_Bn9wHjo_V4afkM';

  uploadFile( filesToUpload: File) {

    const formData = new FormData();
    formData.append('file', filesToUpload, filesToUpload.name);
    return this.http.post<any>(this.url, formData);
  }
}

