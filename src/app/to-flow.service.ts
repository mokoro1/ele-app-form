import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Applicant } from './applicant';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToFlowService {



  url = 'https://prod-76.westeurope.logic.azure.com:443/workflows/3e57b775c1ff4847bd600f2e93e4ec98/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=3DpG7m5scqSRV7bvxMqF2jf7w8Lh_Bn9wHjo_V4afkM';

  constructor(
    private http: HttpClient
  ) { }


  /** POST: Send applicant information to Microsoft Flow */
  toFlow(applicant: Applicant): Observable<Applicant> {
    return this.http.post<any>(this.url, applicant);
  }
}
