import { TestBed } from '@angular/core/testing';

import { ToFlowService } from './to-flow.service';

describe('ToFlowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToFlowService = TestBed.get(ToFlowService);
    expect(service).toBeTruthy();
  });
});
