## Angular Form (English Language Education)
This Angular form is a current Work in Progress web application that uses Angular, an open-source web application framework. The goal of this project is to build a robust and secure web-form application that sends user information to Microsoft Flows on submit, which then sends the information to a secure SharePoint list once it has finished its process on Flows. This project is for the upcoming English Language Education course and we believe that the Angular framework is the ideal tool for the job.

## Getting Started
Prerequisites.
The tools required to run the web app in the testing environment:

Npm 
Angular CLI (installed through npm install -g @angular/cli@latest for the latest version of Angular CLI (as of 22/03/2019, the latest version is 7.3.6)). 
Visual Studio Code. 
The cloned repository.

## Opening the Project
Enter the CLI and go to the directory of the angular application. When you're there, type code ., this command opens up visual studio code with the application's code assets. There you can work on implementing any necessary changes to the project. The actual logic and application of the application itself where most of the configuring will be done will be at angular-test-form\src\app\application-form. These are the source files of the application which contain:

## Running the Development Environment
To test the the application itself, go to the CLI and enter the command ng serve, this command sets up a development server on your localhost, copy/paste the localhost into the browser and your application should be there. If it is the case that ng serve does not run, try using the command npm run ng serve.

## Useful Commands to know for Angular CLI
Code scaffolding
Run ng generate component component-name to generate a new component. You can also use ng generate directive|pipe|service|class|guard|interface|enum|module. A component controls a patch of screen called a view.

## Build
Run ng build to build the project. The build artifacts will be stored in the dist/ directory. Use the --prod flag for a production build.

## Running unit tests
Run ng test to execute the unit tests via Karma.

## Running end-to-end tests
Run ng e2e to execute the end-to-end tests via Protractor.

## Further info
This project was generated with Angular CLI version 7.3.5. To get more help on the Angular CLI use ng help or go check out the Angular CLI README.
This project was generated with Angular version 7.2.8